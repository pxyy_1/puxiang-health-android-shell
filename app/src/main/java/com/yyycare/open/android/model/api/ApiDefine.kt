package com.yyycare.open.android.model.api

class ApiDefine private constructor(
  val appKey: String,
  val apiBaseUrl: String,
  val h5EntryUrl: String,
) {
    companion object {
        private val map = mapOf(
            WorkEnv.PRO to ApiDefine(
                "C0002",
                "https://open.3ycare.com/",
                "https://open.3ycare.com/medical-h5/",
            ),
            WorkEnv.TEST to ApiDefine(
                "C0002",
                "https://open-test.3ycare.com/",
                "https://open-test.3ycare.com/medical-h5/",
            ),
            WorkEnv.DEV to ApiDefine(
                "C0002",
                "https://open-dev.3ycare.com/",
                "https://open-dev.3ycare.com/medical-h5/",
            ),
        )

        fun env(workEnv: WorkEnv): ApiDefine {
            return map[workEnv]!!
        }

        fun currentEnv(): ApiDefine {
            return env(WorkEnv.current)
        }
    }
}
