package com.yyycare.open.android.ui.activity

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import com.yyycare.open.android.ui.dialog.BaseDialog
import com.yyycare.open.android.ui.dialog.LoadingDialog
import com.yyycare.open.android.util.showToast
import org.cnodejs.android.md.vm.holder.ILoadingViewModel
import org.cnodejs.android.md.vm.holder.IToastViewModel

abstract class BaseActivity : AppCompatActivity() {
    protected fun observeBaseViewModel(viewModel: ViewModel) {
        if (viewModel is IToastViewModel) {
            viewModel.toastHolder.toastEvent.observe(this) {
                it?.let { message ->
                    showToast(message)
                }
            }
        }

        if (viewModel is ILoadingViewModel) {
            viewModel.loadingHolder.loadingCountData.observe(this) {
                it?.let { count ->
                    if (count > 0) {
                        LoadingDialog.show(supportFragmentManager, LoadingDialog.TAG)
                    } else {
                        BaseDialog.dismiss(supportFragmentManager, LoadingDialog.TAG)
                    }
                }
            }
        }
    }
}
