package com.yyycare.open.android.model.api

import com.yyycare.open.android.model.entity.ApiResult
import com.yyycare.open.android.model.entity.ExchangeUserTokenRequest
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiService {
    @POST("medical/user/exchangeUserToken")
    suspend fun exchangeUserToken(@Body request: ExchangeUserTokenRequest): ApiResult<String>
}
