package com.yyycare.open.android.model.entity

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiResult<Data>(
    val code: Int,
    val data: Data?,
    @Json(name = "msg") val message: String?,
    val success: Boolean,

) {
    fun isOk(): Boolean {
        return code == 0
    }
}
