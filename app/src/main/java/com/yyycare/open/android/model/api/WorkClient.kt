package com.yyycare.open.android.model.api

import android.content.Context
import androidx.annotation.GuardedBy

class WorkClient private constructor(context: Context) {
    companion object {
        private val lock = Any()

        @GuardedBy("lock")
        @Volatile
        private var instance: WorkClient? = null

        fun with(context: Context) = instance ?: synchronized(lock) {
            instance ?: WorkClient(context.applicationContext).also { instance = it }
        }
    }

    private val map = mapOf(
        WorkEnv.PRO to ApiClient(context, ApiDefine.env(WorkEnv.PRO)),
        WorkEnv.DEV to ApiClient(context, ApiDefine.env(WorkEnv.DEV)),
    )

    fun env(workEnv: WorkEnv): ApiClient {
        return map[workEnv]!!
    }

    fun currentEnv(): ApiClient {
        return env(WorkEnv.current)
    }
}
