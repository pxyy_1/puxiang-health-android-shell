package com.yyycare.open.android.ui.dialog

import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager

abstract class BaseDialog : DialogFragment() {
    companion object {
        fun find(manager: FragmentManager, tag: String): BaseDialog? {
            return manager.findFragmentByTag(tag) as? BaseDialog
        }

        fun dismiss(manager: FragmentManager, tag: String) {
            find(manager, tag)?.dismiss()
        }
    }
}
