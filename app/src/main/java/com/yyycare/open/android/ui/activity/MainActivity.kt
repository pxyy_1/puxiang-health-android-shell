package com.yyycare.open.android.ui.activity

import android.os.Bundle
import android.text.TextUtils
import android.webkit.CookieManager
import android.webkit.WebStorage
import androidx.activity.viewModels
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.yyycare.open.android.R
import com.yyycare.open.android.databinding.ActivityMainBinding
import com.yyycare.open.android.model.api.ApiDefine
import com.yyycare.open.android.model.api.WorkEnv
import com.yyycare.open.android.util.PermissionSettingsPage
import com.yyycare.open.android.util.isPhone
import com.yyycare.open.android.util.isUrl
import com.yyycare.open.android.util.showToast
import com.yyycare.open.android.vm.MainViewModel

class MainActivity : BaseActivity() {
    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        binding.btnSetCurrentEnvH5EntryUrl.setOnClickListener {
            binding.edtUrl.setText(ApiDefine.currentEnv().h5EntryUrl)
        }

        binding.btnClearUrl.setOnClickListener {
            binding.edtUrl.setText("https://")
        }

        binding.btnOpenByUrl.setOnClickListener {
            val url = (binding.edtUrl.text ?: "").trim().toString()
            if (TextUtils.isEmpty(url)) {
                showToast("请输入 URL")
                return@setOnClickListener
            }
            if (!url.isUrl()) {
                showToast("URL 格式不正确")
                return@setOnClickListener
            }
            val isKeepAppBar = binding.switchIsKeepAppBar.isChecked
            WebActivity.open(this, url, isKeepAppBar)
        }

        binding.btnOpenByPhone.setOnClickListener {
            val phone = (binding.edtPhone.text ?: "").trim().toString()
            if (TextUtils.isEmpty(phone)) {
                showToast("请输入手机号")
                return@setOnClickListener
            }
            if (!phone.isPhone()) {
                showToast("手机号格式不正确")
                return@setOnClickListener
            }
            viewModel.login(phone)
        }

        binding.rgEnv.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_env_pro -> WorkEnv.current = WorkEnv.PRO
                R.id.rb_env_test -> WorkEnv.current = WorkEnv.TEST
                R.id.rb_env_dev -> WorkEnv.current = WorkEnv.DEV
            }
        }

        binding.btnClearStorage.setOnClickListener {
            WebStorage.getInstance().deleteAllData()
            showToast("Storage 已清除")
        }

        binding.btnClearCookie.setOnClickListener {
            CookieManager.getInstance().removeAllCookies {
                showToast("Cookie 已清除")
            }
        }

        binding.btnOpenPermissionSettings.setOnClickListener {
            PermissionSettingsPage.goToSettings(this)
        }

        lifecycle.addObserver(object : DefaultLifecycleObserver {
            override fun onResume(owner: LifecycleOwner) {
                when (WorkEnv.current) {
                    WorkEnv.PRO -> binding.rgEnv.check(R.id.rb_env_pro)
                    WorkEnv.TEST -> binding.rgEnv.check(R.id.rb_env_test)
                    WorkEnv.DEV -> binding.rgEnv.check(R.id.rb_env_dev)
                }
            }
        })

        observeBaseViewModel(viewModel)

        viewModel.loginOkEvent.observe(this) {
            it?.let { token ->
                val apiDefine = ApiDefine.currentEnv()
                val url = "${apiDefine.h5EntryUrl}?token=${token}&code=${apiDefine.appKey}"
                val isKeepAppBar = binding.switchIsKeepAppBar.isChecked
                WebActivity.open(this, url, isKeepAppBar)
            }
        }

        setContentView(binding.root)
    }
}
