package com.yyycare.open.android.util

fun String.isUrl(): Boolean {
    return startsWith("https://") or startsWith("http://")
}

fun String.isPhone(): Boolean {
    if (length != 11) {
        return false
    }
    return try {
        toLong()
        true
    } catch (_: Exception) {
        false
    }
}
