package com.yyycare.open.android.ui.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.util.AttributeSet;
import android.webkit.PermissionRequest;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.github.lzyzsd.jsbridge.BridgeWebViewClient;

public class AppWebView extends BridgeWebView {
    @Nullable
    private OnWebClientListener onWebClientListener;

    public AppWebView(@NonNull Context context) {
        super(context);
        init();
    }

    public AppWebView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AppWebView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void init() {
        setWebContentsDebuggingEnabled(true);
        getSettings().setJavaScriptEnabled(true);
        getSettings().setDomStorageEnabled(true);

        setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (onWebClientListener != null) {
                    onWebClientListener.onProgressChanged(newProgress);
                }
            }

            @Override
            public void onPermissionRequest(PermissionRequest request) {
                request.grant(request.getResources());
            }

            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                if (onWebClientListener != null) {
                    return onWebClientListener.onShowFileChooser(filePathCallback, fileChooserParams);
                }
                return super.onShowFileChooser(webView, filePathCallback, fileChooserParams);
            }
        });
    }

    @Override
    protected BridgeWebViewClient generateBridgeWebViewClient() {
        return new BridgeWebViewClient(this) {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                boolean handled = false;
                if (onWebClientListener != null && url != null) {
                    handled = onWebClientListener.shouldOverrideUrlLoading(url);
                }
                if (handled) {
                    return true;
                }
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if (onWebClientListener != null) {
                    onWebClientListener.onPageStarted(url);
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (onWebClientListener != null) {
                    onWebClientListener.onPageFinished(url);
                }
            }

            @SuppressLint("WebViewClientOnReceivedSslError")
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }
        };
    }

    public void setOnWebClientListener(@Nullable OnWebClientListener onWebClientListener) {
        this.onWebClientListener = onWebClientListener;
    }

    public static abstract class OnWebClientListener {
        public boolean shouldOverrideUrlLoading(String url) {
            return false;
        }

        public void onPageStarted(String url) {}

        public void onPageFinished(String url) {}

        public void onProgressChanged(int newProgress) {}

        public boolean onShowFileChooser(ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
            return false;
        }
    }
}
