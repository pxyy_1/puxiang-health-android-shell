package com.yyycare.open.android.model.api

import android.content.Context
import android.os.Build
import com.yyycare.open.android.BuildConfig
import com.yyycare.open.android.util.HttpUtils
import com.yyycare.open.android.util.JsonUtils
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class ApiClient(context: Context, apiDefine: ApiDefine) {
    companion object {
        private const val CACHE_DIR_NAME = "3ycare"
        private val USER_AGENT = "3ycare/${BuildConfig.VERSION_NAME} (Android ${Build.VERSION.RELEASE}; ${Build.MODEL} Build/${Build.ID})"
    }

    val api: ApiService

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(apiDefine.apiBaseUrl)
            .client(OkHttpClient.Builder()
                .addInterceptor { chain ->
                    chain.proceed(chain.request().newBuilder()
                        .header(HttpUtils.HEADER_USER_AGENT, USER_AGENT)
                        .header("Platform", "android")
                        .header("ChannelCode", apiDefine.appKey)
                        .header("ReqSource", "MEDICAL_H5")
                        .build())
                }
                .addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
                .cache(HttpUtils.createCache(context, CACHE_DIR_NAME))
                .build())
            .addConverterFactory(MoshiConverterFactory.create(JsonUtils.moshi))
            .build()
        api = retrofit.create(ApiService::class.java)
    }
}
