package com.yyycare.open.android.ui.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.webkit.ValueCallback
import android.webkit.WebChromeClient
import androidx.activity.OnBackPressedCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.view.isVisible
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.github.lzyzsd.jsbridge.CallBackFunction
import com.yyycare.open.android.BuildConfig
import com.yyycare.open.android.databinding.ActivityWebBinding
import com.yyycare.open.android.ui.widget.AppWebView.OnWebClientListener
import com.yyycare.open.android.util.PermissionSettingsPage
import com.yyycare.open.android.util.showToast
import com.yyycare.open.android.vm.WebViewModel
import java.io.File
import java.util.*

class WebActivity : BaseActivity() {
    companion object {
        private const val TAG = "WebActivity"

        private const val EXTRA_URL = "url"
        private const val EXTRA_IS_KEEP_APP_BAR = "isKeepAppBar"

        fun open(activity: Activity, url: String, isKeepAppBar: Boolean) {
            val intent = Intent(activity, WebActivity::class.java)
                .apply {
                    putExtra(EXTRA_URL, url)
                    putExtra(EXTRA_IS_KEEP_APP_BAR, isKeepAppBar)
                }
            activity.startActivity(intent)
        }
    }

    private lateinit var binding: ActivityWebBinding
    private val viewModel: WebViewModel by viewModels()

    private var currentPermissionCallBackFunction: CallBackFunction? = null
    private var currentFilePathCallback: ValueCallback<Array<Uri>>? = null
    private var currentTakePictureUri: Uri? = null

    private val requestPermissionLauncher = registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
        currentPermissionCallBackFunction?.let { function ->
            function.onCallBack(isGranted.toString())
            currentPermissionCallBackFunction = null
        }
    }

    private val takePictureLauncher = registerForActivityResult(ActivityResultContracts.TakePicture()) { saved ->
        currentFilePathCallback?.let { filePathCallback ->
            if (saved) {
                currentTakePictureUri?.let { uri ->
                    filePathCallback.onReceiveValue(arrayOf(uri))
                    currentTakePictureUri = null
                } ?: run {
                    filePathCallback.onReceiveValue(null)
                }
            } else {
                filePathCallback.onReceiveValue(null)
            }
            currentFilePathCallback = null
        }
    }

    private val getContentLauncher = registerForActivityResult(ActivityResultContracts.GetContent()) { result ->
        currentFilePathCallback?.let { filePathCallback ->
            filePathCallback.onReceiveValue(result?.let { arrayOf(it) })
            currentFilePathCallback = null
        }
    }

    private val getMultipleContentsLauncher = registerForActivityResult(ActivityResultContracts.GetMultipleContents()) { result ->
        currentFilePathCallback?.let { filePathCallback ->
            filePathCallback.onReceiveValue(result?.toTypedArray())
            currentFilePathCallback = null
        }
    }

    private val fileChooserLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { activityResult ->
        currentFilePathCallback?.let { filePathCallback ->
            // 默认实现有 BUG
            // val result = WebChromeClient.FileChooserParams.parseResult(activityResult.resultCode, activityResult.data)
            // 手动实现
            val intent = activityResult.data
            val result = intent?.clipData?.let { clipData ->
                val uris: MutableList<Uri> = mutableListOf()
                for (i in 0 until clipData.itemCount) {
                    val item = clipData.getItemAt(i)
                    uris.add(item.uri)
                }
                uris.toTypedArray()
            } ?: intent?.data?.let { data ->
                arrayOf(data)
            }
            filePathCallback.onReceiveValue(result)
            currentFilePathCallback = null
        }
    }

    private lateinit var homepageUrl: String
    private var isKeepAppBar = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWebBinding.inflate(layoutInflater)

        homepageUrl = intent.getStringExtra(EXTRA_URL)!!
        isKeepAppBar = intent.getBooleanExtra(EXTRA_IS_KEEP_APP_BAR, isKeepAppBar)

        binding.appBar.isVisible = isKeepAppBar

        binding.web.setOnWebClientListener(object : OnWebClientListener() {
            override fun shouldOverrideUrlLoading(url: String): Boolean {
                if (url.startsWith("weixin://")) {
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                    intent.resolveActivity(packageManager)?.let {
                        startActivity(intent)
                    } ?: run {
                        showToast("没有安装微信")
                    }
                    return true
                }
                return super.shouldOverrideUrlLoading(url)
            }

            override fun onPageStarted(url: String) {
                Log.i(TAG, "开始加载路由：$url")
                binding.tvTitle.text = url
            }

            override fun onPageFinished(url: String) {
                binding.tvTitle.text = binding.web.title
            }

            override fun onProgressChanged(newProgress: Int) {
                binding.progressBar.progress = newProgress
                binding.progressBar.isVisible = newProgress < 100
            }

            override fun onShowFileChooser(filePathCallback: ValueCallback<Array<Uri>>, fileChooserParams: WebChromeClient.FileChooserParams): Boolean {
                if (fileChooserParams.isCaptureEnabled) {
                    val acceptTypes = fileChooserParams.acceptTypes
                    if (acceptTypes.size == 1 && acceptTypes[0].startsWith("image/")) {
                        return if (ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                            currentFilePathCallback = filePathCallback
                            val takePictureFile = File.createTempFile(UUID.randomUUID().toString(), ".capture", cacheDir)
                            currentTakePictureUri = FileProvider.getUriForFile(applicationContext, "${BuildConfig.APPLICATION_ID}.FileProvider", takePictureFile)
                            takePictureLauncher.launch(currentTakePictureUri)
                            true
                        } else {
                            showToast("需要相机权限")
                            false
                        }
                    }
                    showToast("不支持的硬件设备类型")
                    return false
                } else {
                    currentFilePathCallback = filePathCallback
                    when (fileChooserParams.mode) {
                        WebChromeClient.FileChooserParams.MODE_OPEN -> {
                            val mimeType = fileChooserParams.acceptTypes.joinToString("|")
                            getContentLauncher.launch(mimeType)
                        }
                        WebChromeClient.FileChooserParams.MODE_OPEN_MULTIPLE -> {
                            val mimeType = fileChooserParams.acceptTypes.joinToString("|")
                            getMultipleContentsLauncher.launch(mimeType)
                        }
                        else -> {
                            fileChooserLauncher.launch(fileChooserParams.createIntent())
                        }
                    }
                    return true
                }
            }
        })

        binding.web.registerHandler("getPrivilege") { data, function ->
            Log.i(TAG, "调用权限查询：${data}")
            val isGranted: Boolean
            if ("photo".equals(data)) {
                isGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
            } else if ("camera".equals(data)) {
                isGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
            } else if ("recorder".equals(data)) {
                isGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
            } else {
                isGranted = false
            }
            Log.i(TAG, "权限：${data} 状态：${isGranted}")
            function.onCallBack(isGranted.toString())
        }

        binding.web.registerHandler("openPrivilege") { data, function ->
            Log.i(TAG, "请求权限：${data}")
            if ("photo".equals(data)) {
                currentPermissionCallBackFunction = function
                requestPermissionLauncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE)
            } else if ("camera".equals(data)) {
                currentPermissionCallBackFunction = function
                requestPermissionLauncher.launch(Manifest.permission.CAMERA)
            } else if ("recorder".equals(data)) {
                currentPermissionCallBackFunction = function
                requestPermissionLauncher.launch(Manifest.permission.RECORD_AUDIO)
            } else {
                function.onCallBack(false.toString())
            }
        }

        binding.web.registerHandler("settingsPrivilege") { _, function ->
            Log.i(TAG, "跳转到权限设置页面")
            PermissionSettingsPage.goToSettings(this)
            function.onCallBack(null)
        }

        binding.btnClose.setOnClickListener {
            finish()
        }

        binding.btnHome.setOnClickListener {
            binding.web.loadUrl(homepageUrl)
        }

        onBackPressedDispatcher.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (binding.web.canGoBack()) {
                    binding.web.goBack()
                } else {
                    finish()
                }
            }
        })

        lifecycle.addObserver(object : DefaultLifecycleObserver {
            override fun onResume(owner: LifecycleOwner) {
                binding.web.onResume()
                binding.web.resumeTimers()
            }

            override fun onPause(owner: LifecycleOwner) {
                binding.web.pauseTimers()
                binding.web.onPause()
            }

            override fun onDestroy(owner: LifecycleOwner) {
                binding.web.destroy()
            }
        })

        observeBaseViewModel(viewModel)

        binding.web.loadUrl(homepageUrl)

        setContentView(binding.root)
    }
}
