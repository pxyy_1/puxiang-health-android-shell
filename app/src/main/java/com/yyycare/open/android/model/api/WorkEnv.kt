package com.yyycare.open.android.model.api

enum class WorkEnv {
    PRO,
    TEST,
    DEV;

    companion object {
        var current = DEV
    }
}
