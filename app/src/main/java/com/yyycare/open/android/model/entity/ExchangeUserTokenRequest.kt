package com.yyycare.open.android.model.entity

import com.squareup.moshi.JsonClass
import com.yyycare.open.android.model.api.ApiDefine

@JsonClass(generateAdapter = true)
data class ExchangeUserTokenRequest(
    val userPhone: String,
    val appKey: String,
    val thirdUserId: String = "0",
)
