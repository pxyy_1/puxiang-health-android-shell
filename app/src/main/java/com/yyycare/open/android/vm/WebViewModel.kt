package com.yyycare.open.android.vm

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import org.cnodejs.android.md.vm.holder.ILoadingViewModel
import org.cnodejs.android.md.vm.holder.IToastViewModel
import org.cnodejs.android.md.vm.holder.LoadingLiveHolder
import org.cnodejs.android.md.vm.holder.ToastLiveHolder

class WebViewModel(application: Application) : AndroidViewModel(application), IToastViewModel, ILoadingViewModel {
    companion object {
        private const val TAG = "WebViewModel"
    }

    override val toastHolder = ToastLiveHolder()
    override val loadingHolder = LoadingLiveHolder()
}
