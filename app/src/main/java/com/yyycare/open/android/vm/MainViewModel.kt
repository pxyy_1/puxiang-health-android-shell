package com.yyycare.open.android.vm

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.hadilq.liveevent.LiveEvent
import com.yyycare.open.android.model.api.ApiDefine
import com.yyycare.open.android.model.api.WorkClient
import com.yyycare.open.android.model.entity.ExchangeUserTokenRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.cnodejs.android.md.vm.holder.ILoadingViewModel
import org.cnodejs.android.md.vm.holder.IToastViewModel
import org.cnodejs.android.md.vm.holder.LoadingLiveHolder
import org.cnodejs.android.md.vm.holder.ToastLiveHolder

class MainViewModel(application: Application) : AndroidViewModel(application), IToastViewModel, ILoadingViewModel {
    companion object {
        private const val TAG = "MainViewModel"
    }

    private val workClient = WorkClient.with(application)

    override val toastHolder = ToastLiveHolder()
    override val loadingHolder = LoadingLiveHolder()
    val loginOkEvent = LiveEvent<String>()

    fun login(phone: String) {
        loadingHolder.showLoading()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val apiDefine = ApiDefine.currentEnv()
                val result = workClient.currentEnv().api.exchangeUserToken(ExchangeUserTokenRequest(phone, apiDefine.appKey))
                delay(500)
                withContext(Dispatchers.Main) {
                    loadingHolder.hideLoading()
                    if (result.isOk()) {
                        loginOkEvent.value = result.data ?: ""
                    } else {
                        toastHolder.showToast(result.message ?: "接口请求错误")
                    }
                }
            } catch (e: Exception) {
                Log.e(TAG, "login", e)
                delay(500)
                withContext(Dispatchers.Main) {
                    loadingHolder.hideLoading()
                    toastHolder.showToast("网络访问错误")
                }
            }
        }
    }
}
